#! /bin/bash

#Ce script prend en argument une arborescence

#Ce script va demander à l'utilisateur une extension (par exemple JpG) et donner le nombre de fichiers avec cette extension dans l'arborescence

echo Veuillez choisir une extension : 
read extension

if [ $# == 1 ]
    then
        echo L\'arborescence existe !
        nbr_fic=$(find $1 -name "*.$extension" | wc -l)
        echo Il y a $nbr_fic fichier de type $extension dans l\'arborescence.
    else
        echo L\'arborescence n\'existe pas
fi