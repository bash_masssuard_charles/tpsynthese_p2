#! /bin/bash

#Ce script prend en argument une extension (par exemple JpG ou PNg)
#et un chemin de fichier

#Ce script va lister tout les fichiers du chemin de fichier ayant pour extension celle donnée par l'utilisateur

if [ $# == 2 ]
    then
        if [ -d $2 ]
            then
                echo Le fichier $2 existe !
                find $2 -name "*.$1" 
            else
                echo Le fichier $2 n\'existe pas !
                mkdir $2
                echo Le fichier $2 a été crée !
        fi
    else
        echo Veuillez rentrer deux arguments
fi
